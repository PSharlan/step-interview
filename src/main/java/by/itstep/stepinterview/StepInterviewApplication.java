package by.itstep.stepinterview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepInterviewApplication {

	public static void main(String[] args) {
		SpringApplication.run(StepInterviewApplication.class, args);
	}

}
